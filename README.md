# todo-api-python

- https://www.zeolearn.com/magazine/creating-a-restful-api-service-with-flask

# Commands
- Signup
  `curl -X POST http://127.0.0.1:5000/v0.1/auth/signup -d 'username=dellinger&password=pass&email=david.ellinger@me.com'`


# Planning
## Server / API
- REST
   - /todo : Get
   - /todo : Post
   - /todo : Put
   - /user/:id : Get
- Authentication
- MySql with SQLAlchemy
